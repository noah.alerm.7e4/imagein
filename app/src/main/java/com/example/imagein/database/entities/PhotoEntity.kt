package com.example.imagein.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PhotoEntity(@PrimaryKey var id: Int)
