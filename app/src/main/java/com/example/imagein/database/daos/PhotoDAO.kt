package com.example.imagein.database.daos

import androidx.room.*
import com.example.imagein.database.entities.PhotoEntity

@Dao
interface PhotoDAO {
    @Query("SELECT * FROM PhotoEntity")
    fun getAllPhotos(): MutableList<PhotoEntity>

    @Insert
    fun addPhoto(photo: PhotoEntity)

    @Delete
    fun deletePhoto(photo: PhotoEntity)
}
