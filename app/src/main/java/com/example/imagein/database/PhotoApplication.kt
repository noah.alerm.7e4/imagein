package com.example.imagein.database

import android.app.Application
import androidx.room.Room

class PhotoApplication : Application() {
    //This is used to create a singleton of the database.
    companion object {
        lateinit var database: PhotoDatabase
    }

    //ON CREATE
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this, PhotoDatabase::class.java,"PhotoDatabase").build()
    }
}
