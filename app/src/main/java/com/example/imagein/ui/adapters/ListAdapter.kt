package com.example.imagein.ui.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.media.MediaPlayer
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.getSystemService
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.imagein.ui.viewModel.ImageInViewModel
import com.example.imagein.R
import com.example.imagein.data.models.Photograph
import com.example.imagein.database.PhotoApplication
import com.example.imagein.database.entities.PhotoEntity
import com.example.imagein.ui.views.ListFragmentDirections
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/*
Context -> Used to load the images, to use the vibration effect and to play the favorites sound.
isFavorites -> Used to remove an item if it is unliked when on the favorites section.
View model -> Used to check if vibration and the sound are on.
*/
class ListAdapter(private val context: Context, private val isFavorites: Boolean, private var viewModel: ImageInViewModel) :
    RecyclerView.Adapter<ListAdapter.ListViewHolder>() {
    //ATTRIBUTES
    private var photographs = mutableListOf<Photograph>()

    /**
     * This method is used to obtain the photos list. (LIVE DATA)
     */
    @SuppressLint("NotifyDataSetChanged")
    fun setPhotographs(photos: List<Photograph>) {
        photographs = photos.sortedBy { it.id }.toMutableList()
        notifyDataSetChanged()
    }

    //ADAPTER METHODS
    /**
     * This method is used to create a View Holder and to set up it's view.
     * @param parent Adapter's parent (used to get context)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.list_recyclerview_item,
            parent, false)

        return ListViewHolder(view)
    }

    /**
     * This method is used to set up the data of each item.
     * @param holder View Holder
     * @param position Current item
     */
    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bindData(photographs[position], context)

        //ANIMATION
        holder.itemView.animation = AnimationUtils.loadAnimation(holder.itemView.context,
            R.anim.list_animation
        )

        //FAVORITE
        val favoriteIcon: ImageView = holder.itemView.findViewById(R.id.favorites)

        //ON CLICK
        //item
        holder.itemView.setOnClickListener {
            val action = ListFragmentDirections.actionListToDetail(photographs[position].id)
            Navigation.findNavController(holder.itemView).navigate(action)
        }

        //favorites
        favoriteIcon.setOnClickListener {
            //ANIMATION
            YoYo.with(Techniques.Tada).duration(300).playOn(favoriteIcon)

            //SOUND (See Settings)
            if (viewModel.soundOn) {
                val mp = MediaPlayer.create(context, R.raw.favorites_sound)
                mp.start()
            }

            //VIBRATION (See Settings)
            if (viewModel.vibrationOn) {
                @Suppress("DEPRECATION")
                getSystemService(context, Vibrator::class.java)?.vibrate(50)
            }

            //DATABASE UPDATE
            if (photographs[position].favorite) {
                CoroutineScope(Dispatchers.Main).launch {
                    withContext(Dispatchers.IO) { PhotoApplication.database.photoDao().deletePhoto(PhotoEntity(photographs[position].id)) }
                }
            }
            else {
                CoroutineScope(Dispatchers.Main).launch {
                    withContext(Dispatchers.IO) { PhotoApplication.database.photoDao().addPhoto(PhotoEntity(photographs[position].id)) }
                }
            }

            //UPDATE
            photographs[position].favorite = !photographs[position].favorite
            holder.setFavoriteIcon(photographs[position].favorite)

            //When on the favorites section, the item must be removed from the recycler view.
            if (isFavorites) {
                notifyItemRemoved(position)
                Navigation.findNavController(holder.itemView).navigate(R.id.action_list_self)
            }
        }
    }

    /**
     * This method is used to get the total amount of items in the Recycler View.
     */
    override fun getItemCount(): Int {
        return photographs.size
    }


    //VIEW HOLDER
    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //ATTRIBUTES
        private var image: ImageView = itemView.findViewById(R.id.image)
        private var author: TextView = itemView.findViewById(R.id.author)
        private var width: TextView = itemView.findViewById(R.id.width)
        private var height: TextView = itemView.findViewById(R.id.height)
        private var favoriteIcon: ImageView = itemView.findViewById(R.id.favorites)

        //METHODS
        /**
         * This method is used to set up the data of each item of the photographs list.
         */
        @SuppressLint("SetTextI18n")
        fun bindData(photo: Photograph, context: Context) {
            //Picasso.get().load(photo.download_url).error(R.drawable.error_loading).into(image)
            Glide.with(context).load(photo.download_url).error(R.drawable.error_loading).into(image)
            author.text = photo.author
            width.text = photo.width.toString()
            height.text = photo.height.toString()

            setFavoriteIcon(photo.favorite)
        }

        /**
         * This method is used to set the image of the favorites icon.
         */
        fun setFavoriteIcon(isFavorite: Boolean) {
            if (isFavorite)
                favoriteIcon.setImageResource(R.drawable.favorite_icon_filled)
            else
                favoriteIcon.setImageResource(R.drawable.favorite_icon)
        }
    }
}
