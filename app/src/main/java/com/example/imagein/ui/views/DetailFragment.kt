package com.example.imagein.ui.views

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Vibrator
import android.view.View
import android.view.animation.AnimationUtils.loadAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.imagein.ui.viewModel.ImageInViewModel
import com.example.imagein.R
import com.example.imagein.data.models.Photograph
import com.example.imagein.database.PhotoApplication
import com.example.imagein.database.entities.PhotoEntity
import com.example.imagein.ui.adapters.DetailAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailFragment : Fragment(R.layout.fragment_detail) {
    //ATTRIBUTES
    //layout
    private lateinit var photoView: ImageView
    private lateinit var goBackButton: ImageView
    private lateinit var id: TextView
    private lateinit var favoriteIcon: ImageView
    private lateinit var author: TextView
    private lateinit var url: TextView
    private lateinit var width: TextView
    private lateinit var height: TextView
    private lateinit var downloadURL: TextView
    private lateinit var byAuthor: TextView
    private lateinit var recyclerView: RecyclerView

    //photo management
    private lateinit var photos: List<Photograph>
    private lateinit var filteredPhotos: List<Photograph>
    private lateinit var photo: Photograph

    //view model
    private val viewModel: ImageInViewModel by activityViewModels()

    //ON VIEW CREATED
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //ENTER ANIMATION
        if (!viewModel.comesFromDetail)
            view.startAnimation(loadAnimation(requireContext(), R.anim.detail_enter_animation))

        //IDs
        photoView = view.findViewById(R.id.photo)
        goBackButton = view.findViewById(R.id.back_arrow)
        id = view.findViewById(R.id.photo_id)
        favoriteIcon = view.findViewById(R.id.favorites)
        author = view.findViewById(R.id.author)
        url = view.findViewById(R.id.url_web)
        width = view.findViewById(R.id.width)
        height = view.findViewById(R.id.height)
        downloadURL = view.findViewById(R.id.url_download)
        byAuthor = view.findViewById(R.id.by_author)
        recyclerView = view.findViewById(R.id.recycler_view_detail)

        //LISTS
        viewModel.getPhotographs().observe(viewLifecycleOwner, { setPhotographs(it)}) //LIVE DATA

        //If the Handler is removed the app crashes saying the photos have not been initialized (NEEDS FURTHER INSPECTION)
        Handler(Looper.getMainLooper()).postDelayed({
            //PHOTO
            photo = photos.filter { p -> p.id == arguments!!.getInt("photo_id") }[0] //This is used to get an element of the list by its id.

            //FILTERED LIST
            viewModel.getPhotographs().observe(viewLifecycleOwner, { setFilteredPhotographs(
                it.filter { p -> p.author == photo.author })}) //LIVE DATA

            //DATA ASSIGNMENT
            //Picasso.get().load(photo.download_url).error(R.drawable.error_loading).into(photoView)
            Glide.with(requireContext()).load(photo.download_url).error(R.drawable.error_loading).into(photoView)
            id.text = "#${photo.id}"
            setFavoriteIcon(photo.favorite)
            author.text = photo.author
            url.text = photo.url
            width.text = photo.width.toString()
            height.text = photo.height.toString()
            downloadURL.text = "Download here: ${photo.download_url}"
            byAuthor.text = "By ${photo.author}:"

            //RECYCLER VIEW
            recyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            recyclerView.adapter = DetailAdapter(filteredPhotos.sortedBy { it.id }, requireContext(), photo.id, viewModel)
        }, 0)

        //ON CLICK
        //go back button
        goBackButton.setOnClickListener {
            //ANIMATION
            view.startAnimation(loadAnimation(requireContext(), R.anim.detail_exit_animation))

            //NAVIGATION
            Navigation.findNavController(view).navigate(R.id.action_detail_to_list)
        }

        //favorites icon
        favoriteIcon.setOnClickListener {
            //ANIMATION
            YoYo.with(Techniques.Tada).duration(300).playOn(favoriteIcon)

            //SOUND (See Settings)
            if (viewModel.soundOn) {
                val mp = MediaPlayer.create(context, R.raw.favorites_sound)
                mp.start()
            }

            //VIBRATION (See Settings)
            if (viewModel.vibrationOn) {
                @Suppress("DEPRECATION")
                ContextCompat.getSystemService(requireContext(), Vibrator::class.java)?.vibrate(50)
            }

            //DATABASE UPDATE
            if (photo.favorite) {
                CoroutineScope(Dispatchers.Main).launch {
                    withContext(Dispatchers.IO) { PhotoApplication.database.photoDao().deletePhoto(
                        PhotoEntity(photo.id)
                    ) }
                }
            }
            else {
                CoroutineScope(Dispatchers.Main).launch {
                    withContext(Dispatchers.IO) { PhotoApplication.database.photoDao().addPhoto(
                        PhotoEntity(photo.id)
                    ) }
                }
            }

            //UPDATE
            photo.favorite = !photo.favorite

            setFavoriteIcon(photo.favorite)

            (recyclerView.adapter as DetailAdapter).notifyItemChanged(filteredPhotos.indexOf(photo))
        }
    }

    //METHODS
    /**
     * This method is used to set the image of the favorites icon.
     */
    private fun setFavoriteIcon(isFavorite: Boolean) {
        if (isFavorite)
            favoriteIcon.setImageResource(R.drawable.favorite_icon_filled)
        else
            favoriteIcon.setImageResource(R.drawable.favorite_icon)
    }

    /**
     * This method is used to set the photos list from the mutable live data.
     */
    private fun setPhotographs(photographs: List<Photograph>) {
        photos = photographs
    }

    /**
     * This method is used to set the filtered photos list from the mutable live data.
     */
    private fun setFilteredPhotographs(photographs: List<Photograph>) {
        filteredPhotos = photographs
    }
}
