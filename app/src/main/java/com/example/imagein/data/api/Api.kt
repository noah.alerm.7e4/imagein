package com.example.imagein.data.api

import com.example.imagein.data.models.Photograph
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    companion object {
        //API CALL
        val retrofit: Retrofit = Retrofit.Builder().baseUrl("https://picsum.photos/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().build())
            .build()
    }

    //GETTER
    @GET("v2/list")
    suspend fun getPhotographs(@Query("page") num: Int): Response<List<Photograph>>
}
