package com.example.imagein.data.repository

import com.example.imagein.data.api.Api

class Repository {
    //ATTRIBUTES
    private val call = Api.retrofit.create(Api::class.java)

    //METHODS
    /**
     * This method calls the photograph list GET request of the API.
     */
    suspend fun getPhotographs(page: Int) = call.getPhotographs(page)
}
